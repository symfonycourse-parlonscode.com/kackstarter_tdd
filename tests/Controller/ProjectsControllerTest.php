<?php

namespace App\Tests\Controller;

use App\Entity\Projetcs;
use DateTime;
use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProjectsControllerTest extends WebTestCase
{
    private $em;
    private $client;

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->em = static::$container->get('doctrine')->getManager();

        parent::setUp();

        $schemaTool = new SchemaTool($this->em);
        $schemaTool->dropDatabase();

        $metadata = $this->em->getMetadataFactory()->getAllMetadata();
    
        $schemaTool->createSchema($metadata);
    }

    public function test_index_should_list_all_projects()
    {
        $project1 = (new Projetcs)
            ->setName('Symfony Conférence')
            ->setDescription('the best Symfony conférence')
            ->setWebsite('www.google.com')
            ->setTargetAmount('20000')
            ->setExpiredOn(new DateTime('+ 15 days'));

            $project2 = (new Projetcs)
            ->setName('Symfony Conférence')
            ->setDescription('the best Symfony conférence')
            ->setWebsite('www.google.com')
            ->setTargetAmount('20000')
            ->setExpiredOn(new DateTime('+ 15 days'));

            $project3 = (new Projetcs)
            ->setName('Symfony Conférence')
            ->setDescription('the best Symfony conférence')
            ->setWebsite('www.google.com')
            ->setTargetAmount('20000')
            ->setExpiredOn(new DateTime('+ 15 days'));
        


        $this->em->persist($project1);
        $this->em->persist($project2);
        $this->em->persist($project3);
        $this->em->flush();

        $this->client->request('GET', '/projects');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', '3 Projects');
        $this->assertSelectorTextContains('body', $project1->getName());
        $this->assertSelectorTextContains('body', $project2->getName());
        $this->assertSelectorTextContains('body', $project3->getName());
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->em->close();
        $this->em = null;
    }
}

<?php

namespace App\Repository;

use App\Entity\Projetcs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Projetcs|null find($id, $lockMode = null, $lockVersion = null)
 * @method Projetcs|null findOneBy(array $criteria, array $orderBy = null)
 * @method Projetcs[]    findAll()
 * @method Projetcs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjetcsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Projetcs::class);
    }

    // /**
    //  * @return Projetcs[] Returns an array of Projetcs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Projetcs
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

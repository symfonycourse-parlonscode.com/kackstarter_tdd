<?php

namespace App\Controller;

use App\Repository\ProjetcsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ProjectsController extends AbstractController
{
    /**
     * @Route("/projects", name="projects")
     */
    public function index(ProjetcsRepository $repo)
    {
        $projects = $repo->findAll();
        return $this->render('projects/index.html.twig', compact('projects'));
    }
}
